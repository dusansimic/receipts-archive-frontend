import React, {useEffect} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col} from 'react-bootstrap';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	useLocation
} from 'react-router-dom';
import TopBar from './components/TopBar';
import Home from './routes/Home';
import Receipt from './routes/Receipt';
import Stats from './routes/Stats';
import CreateLocation from './routes/CreateLocation';
import CreateReceipt from './routes/CreateReceipt';
import EditReceipt from './routes/EditReceipt';

function Switcher() {
	const location = useLocation();

	useEffect(() => {
		// A console.log(location);
	}, [location]);

	return (
		<Switch>
			<Route path='/new/location' component={CreateLocation}/>
			<Route path='/new/receipt' component={CreateReceipt}/>
			<Route path='/edit/receipt/:id' component={EditReceipt}/>
			<Route path='/receipt/:id' component={Receipt}/>
			<Route path='/stats' component={Stats}/>
			<Route path='/' component={Home}/>
		</Switch>
	);
}

export default function App() {
	return (
		<div className='App'>
			<Router>
				<Container>
					<Row>
						<Col/>
						<Col md={10}>
							<TopBar/>
						</Col>
						<Col/>
					</Row>
					<Row>
						<Col/>
						<Col md={10}>
							<Switcher/>
						</Col>
						<Col/>
					</Row>
				</Container>
			</Router>
		</div>
	);
}
