import React, {useState} from 'react';
import useDeepCompareEffect from 'use-deep-compare-effect';
import PropTypes from 'prop-types';
import {isEmpty} from 'lodash';
import ky from 'ky';
import {Table, Button} from 'react-bootstrap';
import EditReceiptItem from '../components/EditReceiptItem';
import AddReceiptItemModal from '../components/AddReceiptItemModal';

async function fetchItems(setItems, id) {
	setItems(await ky.get('http://localhost:3001/items', {
		searchParams: {
			receiptId: id,
			ids: true
		}
	}).json());
}

// A dependency function for items
// It just returns and object if items isn't that already
const itemsDependency = items => items instanceof Array ? items : [];

export default function EditReceipt(props) {
	const [items, setItems] = useState();
	const [showModal, setShowModal] = useState(false);

	useDeepCompareEffect(() => {
		fetchItems(setItems, props.match.params.id);
	}, [itemsDependency(items)]);

	async function onSaveFunction(data) {
		console.log(data);
		await ky.put('http://localhost:3001/items', {
			json: data
		});
	}

	function invertShowModal() {
		setShowModal(!showModal);
	}

	return (
		<div>
			<AddReceiptItemModal hasShowModal={showModal} whenClose={invertShowModal} receiptId={props.match.params.id}/>
			<Table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Amount</th>
						<th>Price</th>
						<th/>
					</tr>
				</thead>
				<tbody>
					{!isEmpty(items) && items.map(item => (
						<EditReceiptItem key={item.id} id={item.id} name={item.name} amount={item.amount} price={item.price} onSave={onSaveFunction}/>
					))}
				</tbody>
			</Table>
			<Button size='sm' onClick={invertShowModal}>Add new item</Button>
		</div>
	);
}

EditReceipt.propTypes = {
	match: PropTypes.object.isRequired
};
