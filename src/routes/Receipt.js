import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import useDeepCompareEffect from 'use-deep-compare-effect';
import PropTypes from 'prop-types';
import {isEmpty} from 'lodash';
import ky from 'ky';
import ReceiptCard from '../components/ReceiptCard';
import {Container, Row, Col, Button} from 'react-bootstrap';
import './Receipt.css';

async function fetchReceipt(setReceipt, id) {
	setReceipt(await ky.get('http://localhost:3001/receipts', {
		searchParams: {
			locationData: true,
			receiptId: id
		}
	}).json());
}

async function fetchItems(setItems, id) {
	setItems(await ky.get('http://localhost:3001/items', {
		searchParams: {
			receiptId: id,
			ids: true
		}
	}).json());
}

// A dependency function for receipts
// It just returns and object if receipt isn't that already
const receiptDependency = receipt => receipt instanceof Object ? receipt : {};
// Works the same for items dependency
const itemsDependency = items => items instanceof Array ? items : [];

export default function Receipt(props) {
	const [receipt, setReceipt] = useState();
	const [items, setItems] = useState();
	const history = useHistory();

	useDeepCompareEffect(() => {
		fetchReceipt(setReceipt, props.match.params.id);
		fetchItems(setItems, props.match.params.id);
	}, [receiptDependency(receipt), itemsDependency(items)]);

	async function removeReceipt() {
		try {
			const response = await ky.delete('http://localhost:3001/receipts', {
				json: {id: props.match.params.id}
			});
			console.log(response);
			history.push('/');
		} catch (error) {
			console.error(error);
		}
	}

	return isEmpty(receipt) ? <p>Loading...</p> : (
		<Container>
			<Row>
				<Col xs lg={4}>
					<ReceiptCard receipt={receipt} items={items}/>
				</Col>
				<Col>
					<Button href={`/edit/receipt/${props.match.params.id}`}>Edit</Button>
					<Button variant='danger' onClick={removeReceipt}>Remove</Button>
				</Col>
			</Row>
		</Container>
	);
}

Receipt.propTypes = {
	match: PropTypes.object.isRequired
};
