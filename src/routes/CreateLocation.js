import React, {useState} from 'react';
import useDeepCompareEffect from 'use-deep-compare-effect';
import ky from 'ky';
import {Form, Button, Spinner} from 'react-bootstrap';
import {useForm} from 'react-hook-form';
import * as yup from 'yup';

async function fetchLocations(setLocations) {
	setLocations(await ky.get('http://localhost:3001/locations').json());
}

async function postLocation(location) {
	try {
		await ky.post('http://localhost:3001/locations', {
			json: location
		});
	} catch (error) {
		console.error(error);
	}
}

export default function CreateLocation() {
	const SCHEMA = yup.object().shape({
		name: yup.string().test('unique-name', 'The name already exists!', value => !(locations.map(location => location.name).includes(value))).required(),
		address: yup.string().required()
	});

	const {handleSubmit, register, errors} = useForm({
		validationSchema: SCHEMA
	});

	const onSubmit = data => postLocation(data);

	const [locations, setLocations] = useState();

	useDeepCompareEffect(() => {
		fetchLocations(setLocations);
	}, [locations instanceof Array ? locations : []]);

	return locations instanceof Array ? (
		<Form onSubmit={handleSubmit(onSubmit)}>
			<Form.Group controlId='name'>
				<Form.Label>Name</Form.Label>
				<Form.Control ref={register} name='name'></Form.Control>
				{errors.name && 'You must enter a location name!'}
			</Form.Group>

			<Form.Group controlId='address'>
				<Form.Label>Address</Form.Label>
				<Form.Control ref={register} name='address'></Form.Control>
				{errors.address && 'You must enter an address!'}
			</Form.Group>

			<Button variant='primary' type='submit'>Create</Button>
		</Form>
	) : <center><Spinner animation='border' variant='dark'/></center>;
}
