import React, {useState} from 'react';
import useDeepCompareEffect from 'use-deep-compare-effect';
import ky from 'ky';
import {Form, Button, Spinner} from 'react-bootstrap';
import InputMask from 'react-input-mask';
import {useForm} from 'react-hook-form';
import * as yup from 'yup';

async function fetchLocations(setLocations) {
	setLocations(await ky.get('http://localhost:3001/locations').json());
}

async function postReceipt(receipt) {
	try {
		await ky.post('http://localhost:3001/receipts', {
			json: receipt
		});
	} catch (error) {
		console.error(error);
	}
}

export default function CreateReceipt() {
	const SCHEMA = yup.object().shape({
		location: yup.string().required(),
		datetime: yup.date().required()
	});

	const {handleSubmit, register, errors} = useForm({
		validationSchema: SCHEMA
	});

	const onSubmit = data => postReceipt(data);

	const [locations, setLocations] = useState();

	useDeepCompareEffect(() => {
		fetchLocations(setLocations);
	}, [locations instanceof Array ? locations : []]);

	return locations instanceof Array ? (
		<Form onSubmit={handleSubmit(onSubmit)}>
			<Form.Group controlId='location'>
				<Form.Label>Location</Form.Label>
				<Form.Control
					ref={register}
					as='select'
					name='location'
				>
					{locations.map(location => <option key={location.id} value={location.id}>{location.name} @ {location.address}</option>)}
				</Form.Control>
				{errors.location && 'Valid location must be specified!'}
			</Form.Group>

			<Form.Group controlId='datetime'>
				<Form.Label>Date and time</Form.Label>
				<InputMask mask='9999-99-99 99:99:99'>
					<Form.Control ref={register} name='datetime'></Form.Control>
				</InputMask>
				{errors.datetime && 'Valid date must be specified!'}
			</Form.Group>

			<Button variant='primary' type='submit'>Create</Button>
		</Form>
	) : <center><Spinner animation='border' variant='dark'/></center>;
}
