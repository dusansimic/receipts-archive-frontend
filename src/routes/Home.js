import React, {Component} from 'react';
import ky from 'ky';
import ReceiptList from '../components/ReceiptList';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {receipts: []};
	}

	async componentDidMount() {
		try {
			const receipts = await ky.get('http://localhost:3001/receipts?locationNames=true&locationData=true').json();
			console.log(receipts);
			this.setState({receipts});
		} catch (error) {
			console.error(error);
		}
	}

	render() {
		return (
			<div>
				<h4>Receipts</h4>
				<ReceiptList receipts={this.state.receipts instanceof Array ? this.state.receipts : []}/>
			</div>
		);
	}
}

export default Home;
