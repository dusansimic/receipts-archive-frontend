import React, {Component} from 'react';
import {isBrowser, isFirefox, isSafari, isIE} from 'react-device-detect';
import {Form, Col} from 'react-bootstrap';

const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

class DateTimeInput extends Component {
	constructor(props) {
		super(props);

		this.setState({
			date: new Date(this.props.defaultDate)
		});

		this.handleInputChange = this.handleInputChange.bind(this);
	}

	render() {
		if (this.compatableDateInput()) {
			return (
				<Form.Group controlId='datetime'>
					<Form.Label>Date and time</Form.Label>
					<Form.Control type='datetime-local' placeholder='mm/dd/yyyy, --:-- --' value={this.state.date}></Form.Control>
				</Form.Group>
			);
		} else {
			return (
				<Form.Row>
					<Form.Group as={Col} controlId='day'>
						<Form.Label>Day</Form.Label>
						{/* <Form.Control type='number' placeholder='dd' min={1} max={31} value={1}></Form.Control> */}
						<Form.Control as='select' onChange={this.handleInputChange}>
							{this.range(31).map(day => <option key={day}>{day}</option>)}
						</Form.Control>
					</Form.Group>
					<Form.Group as={Col} controlId='month' onChange={this.handleInputChange}>
						<Form.Label>Month</Form.Label>
						<Form.Control as='select'>
							{MONTHS.map(month => <option key={month}>{month}</option>)}
						</Form.Control>
					</Form.Group>
					<Form.Group as={Col} controlId='year'>
						<Form.Label>Year</Form.Label>
						{/* <Form.Control type='number' placeholder='yyyy' min={1970} max={2038} value={1970}></Form.Control> */}
						<Form.Control as='select'>
							{this.range(1970, 2038).map(year => <option key={year}>{year}</option>)}
						</Form.Control>
					</Form.Group>
					<Form.Group as={Col} controlId='hour'>
						<Form.Label>Hour</Form.Label>
						{/* <Form.Control type='number' placeholder='hh' min={0} max={23} value={0}></Form.Control> */}
						<Form.Control as='select'>
							{this.range(0, 23).map(hour => <option key={hour}>{hour}</option>)}
						</Form.Control>
					</Form.Group>
					<Form.Group as={Col} controlId='minute'>
						<Form.Label>Minute</Form.Label>
						{/* <Form.Control type='number' placeholder='mm' min={0} max={59} value={0}></Form.Control> */}
						<Form.Control as='select'>
							{this.range(0, 59).map(minute => <option key={minute}>{minute}</option>)}
						</Form.Control>
					</Form.Group>
					<Form.Group as={Col} controlId='second'>
						<Form.Label>Second</Form.Label>
						{/* <Form.Control type='number' placeholder='ss' min={0} max={59} value={0}></Form.Control> */}
						<Form.Control as='select'>
							{this.range(0, 59).map(second => <option key={second}>{second}</option>)}
						</Form.Control>
					</Form.Group>
				</Form.Row>
			);
		}
	}

	range(start, end) {
		const range = [];
		if (end === undefined) {
			for (let i = 1; i <= start; i++) {
				range.push(i);
			}

			return range;
		}

		for (let i = start; i <= end; i++) {
			range.push(i);
		}

		return range;
	}

	compatableDateInput() {
		return !(isBrowser && (isFirefox || isSafari || isIE));
	}

	handleInputChange(e) {
		if (this.compatableDateInput()) {
			const date = new Date(e.target.value);
			this.setState({date})
			this.props.onDateTimeChange(date);
		}
	}
}

export default DateTimeInput;
