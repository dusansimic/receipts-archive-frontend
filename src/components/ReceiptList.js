import React from 'react';
import PropTypes from 'prop-types';
import {Table} from 'react-bootstrap';
import ReceiptListItem from './ReceiptListItem';

export default function ReceiptList(props) {
	function byDate(receipt1, receipt2) {
		const key1 = new Date(receipt1.createdAt);
		const key2 = new Date(receipt2.createdAt);
		if (key1 < key2) {
			return 1;
		}

		if (key1 > key2) {
			return -1;
		}

		return 0;
	}

	return (
		<Table striped hover>
			<thead>
				<tr>
					<th>Price</th>
					<th>Location</th>
					<th>Time</th>
				</tr>
			</thead>
			<tbody>
				{props.receipts.sort(byDate).map(receipt => (
					<ReceiptListItem key={receipt.id} id={receipt.id} location={receipt.location} createdAt={receipt.createdAt} cost={parseFloat(receipt.cost.toFixed(2))}/>
				))}
			</tbody>
		</Table>
	);
}

ReceiptList.propTypes = {
	receipts: PropTypes.array.isRequired
};
