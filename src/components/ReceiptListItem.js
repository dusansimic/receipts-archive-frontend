import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import {Link} from 'react-router-dom';

function join(...elems) {
	return [...elems].join('');
}

export default function ReceiptListItem(props) {
	return (
		<tr>
			<td><Link to={join('/receipt/', props.id)}>{props.cost} rsd</Link></td>
			<td>{props.location.name}</td>
			<td><Moment fromNow>{props.createdAt}</Moment></td>
		</tr>
	);
}

ReceiptListItem.propTypes = {
	id: PropTypes.string.isRequired,
	location: PropTypes.object.isRequired,
	createdAt: PropTypes.string.isRequired,
	cost: PropTypes.number.isRequired
};
