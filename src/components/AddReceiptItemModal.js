import React from 'react';
import {useForm} from 'react-hook-form';
import * as yup from 'yup';
import PropTypes from 'prop-types';
import {Modal, Button, Form, Row, Col} from 'react-bootstrap';
import ky from 'ky';

export default function AddReceiptItemModal(props) {
	const SCHEMA = yup.object().shape({
		name: yup.string().required(),
		price: yup.number().required(),
		amount: yup.number().required()
	});

	const {handleSubmit, register, errors} = useForm({
		validationSchema: SCHEMA
	});

	async function postItem(item) {
		try {
			await ky.post('http://localhost:3001/items', {
				json: {receiptId: props.receiptId, ...item}
			});
			handleClose();
		} catch (error) {
			console.error(error);
		}
	}

	const onSubmit = data => postItem(data);

	function handleClose() {
		props.whenClose();
	}

	return (
		<Modal show={props.hasShowModal} onHide={handleClose}>
			<Modal.Header>
				<Modal.Title>Add item</Modal.Title>
			</Modal.Header>
			<Form onSubmit={handleSubmit(onSubmit)} onReset={handleClose}>
				<Modal.Body>
					<Form.Group controlId='name'>
						<Form.Label>Name</Form.Label>
						<Form.Control
							ref={register}
							name='name'
							isInvalid={errors.name}
						/>
						<Form.Control.Feedback type='invalid'>
							Valid name must be specified
						</Form.Control.Feedback>
					</Form.Group>

					<Row>
						<Col>
							<Form.Group controlId='price'>
								<Form.Label>Price</Form.Label>
								<Form.Control
									ref={register}
									name='price'
									type='number'
									step={0.01}
									isInvalid={errors.price}
								/>
								<Form.Control.Feedback type='invalid'>
									Price must be a number
								</Form.Control.Feedback>
							</Form.Group>
						</Col>
						<Col>
							<Form.Group controlId='amount'>
								<Form.Label>Amount</Form.Label>
								<Form.Control
									ref={register}
									name='amount'
									type='number'
									step={0.01}
									isInvalid={errors.amount}
								/>
								<Form.Control.Feedback type='invalid'>
									Item amount must be a number
								</Form.Control.Feedback>
							</Form.Group>
						</Col>
					</Row>
				</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' type='reset'>Cancel</Button>
					<Button variant='primary' type='submit'>Add</Button>
				</Modal.Footer>
			</Form>
		</Modal>
	);
}

AddReceiptItemModal.propTypes = {
	hasShowModal: PropTypes.bool.isRequired,
	whenClose: PropTypes.func.isRequired,
	receiptId: PropTypes.string.isRequired
};
