import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Button, Form, Row, Col} from 'react-bootstrap';
import {useForm} from 'react-hook-form';
import * as yup from 'yup';

export default function EditReceiptItem(props) {
	const SCHEMA = yup.object().shape({
		name: yup.string().required(),
		amount: yup.number().required(),
		price: yup.number().required()
	});

	const [editing, setEditing] = useState();
	const {handleSubmit, register, errors} = useForm({
		validationSchema: SCHEMA,
		defaultValues: {
			name: props.name,
			amount: props.amount,
			price: props.price
		}
	});

	function invertEditing() {
		setEditing(!editing);
	}

	function saveItem(data) {
		props.onSave({id: props.id, ...data});
		invertEditing();
	}

	useEffect(() => {}, [editing]);

	return editing ? (
		<tr>
			<td colSpan={4}>
				<Form onSubmit={handleSubmit(saveItem)}>
					<Row>
						<Col>
							<Form.Control
								ref={register}
								size='sm'
								placeholder='Item name'
								name='name'
							/>
							{errors.name && 'Valid name must be specified!'}
						</Col>
						<Col>
							<Form.Control
								ref={register}
								size='sm'
								placeholder='Item amount'
								type='number'
								step={0.01}
								name='amount'
							/>
							{errors.amount && 'Valid amount must be specified!'}
						</Col>
						<Col>
							<Form.Control
								ref={register}
								size='sm'
								placeholder='Item price'
								type='number'
								step={0.01}
								name='price'
							/>
							{errors.price && 'Valid price must be specified!'}
						</Col>
						<Col>
							<Button size='sm' style={{marginRight: '2px'}} type='submit'>Save</Button>
							<Button size='sm' variant='secondary' onClick={invertEditing}>Cancel</Button>
						</Col>
					</Row>
				</Form>
			</td>
		</tr>
	) : (
		<tr>
			<td>{props.name}</td>
			<td>{props.amount}</td>
			<td><strong>{props.price * props.amount} rsd</strong> (single: {props.price} rsd)</td>
			<td><Button size='sm' onClick={invertEditing}>Edit</Button></td>
		</tr>
	);
}

EditReceiptItem.propTypes = {
	id: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	amount: PropTypes.number.isRequired,
	price: PropTypes.number.isRequired,
	onSave: PropTypes.func.isRequired
};
