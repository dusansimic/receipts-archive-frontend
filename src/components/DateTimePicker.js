import React, {Component} from 'react';
import Datetime from 'react-datetime';
import {InputGroup, FormControl, Button} from 'react-bootstrap';

class DateTimePicker extends Component {
	render() {
		return <Datetime rednerInput={this.renderInput}/>;
	}

	renderInput(props, openCalendar, closeCalendar) {
		function clear() {
			props.onChange({target: {value: ''}});
		}

		return (
			<div>
				<InputGroup>
					<FormControl as='input' value={{...props}}></FormControl>
					<InputGroup.Append>
						<Button onClick={openCalendar}>Pick</Button>
					</InputGroup.Append>
				</InputGroup>
			</div>
		);
	}
}

export default DateTimePicker;
