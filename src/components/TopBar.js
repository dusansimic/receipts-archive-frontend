import React from 'react';
import './TopBar.css';
import {Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap';

function TopBar() {
	return (
		<Navbar variant='light' expand='md' className='TopBar' style={{backgroundColor: 'white'}}>
			<Navbar.Brand href='/'>Receipts Archive</Navbar.Brand>
			<Navbar.Toggle aria-controls='basic-navbar-nav'/>
			<Navbar.Collapse id='basic-navbar-nav'>
				<Nav className='mr-auto'>
					<Nav.Link href='/'>Home</Nav.Link>
					<Nav.Link href='/stats'>Stats</Nav.Link>
				</Nav>
				<Form inline>
					<FormControl type='text' placeholder='Search' className='mr-sm-2'/>
					<Button variant='outline-dark'>Search</Button>
				</Form>
				<NavDropdown title='New' id='new-nav-dropdown' variant='dark'>
					<NavDropdown.Item href='/new/location'>Location</NavDropdown.Item>
					<NavDropdown.Item href='/new/receipt'>Receipt</NavDropdown.Item>
				</NavDropdown>
			</Navbar.Collapse>
		</Navbar>
	);
}

export default TopBar;
