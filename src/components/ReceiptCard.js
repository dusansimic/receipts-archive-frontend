import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import {Card} from 'react-bootstrap';
import './ReceiptCard.css';

export default function ReceiptCard(props) {
	return (
		<Card className='text-center' style={{width: '18rem'}}>
			<Card.Body>
				<Card.Title>{props.receipt.location.name}</Card.Title>
				<Card.Subtitle>{props.receipt.location.address}</Card.Subtitle>
				<hr/>
				<table className='receiptData'>
					<tbody>
						<tr>
							<td>Date:</td>
							<td><Moment format='DD. MM. YYYY.'>{props.receipt.createdAt}</Moment></td>
						</tr>
						<tr>
							<td>Time:</td>
							<td><Moment format='HH:mm'>{props.receipt.createdAt}</Moment></td>
						</tr>
					</tbody>
				</table>
				<hr/>
				<table className='receiptData'>
					<tbody>
						{props.items !== undefined && props.items.map(item => (
							<tr key={item.id}>
								<td>{item.name}</td>
								<td>{item.price} rsd</td>
							</tr>
						))}
					</tbody>
				</table>
				<hr/>
				<table className='receiptData'>
					<tbody>
						<tr>
							<td/>
							<td><span style={{fontSize: '1.5em', fontWeight: 500}}>{props.receipt.cost.toFixed(2)}</span> rsd</td>
						</tr>
					</tbody>
				</table>
			</Card.Body>
		</Card>
	);
}

ReceiptCard.propTypes = {
	receipt: PropTypes.object.isRequired,
	items: PropTypes.array
};

ReceiptCard.defaultProps = {
	items: undefined
};
